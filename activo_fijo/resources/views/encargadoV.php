<body style="width: 100%; font-size: 100%; background-color: skyblue;">
	<br><br><br>
	<div class="container">
		<div align="center">
			<label for="" style="color: white; font-weight: 900; font-size: 200%; font-family: arail">Encargados</label>
		</div>
	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  	Ingresar encargado
	</button>
	<br><br>
	<div class="table-responsive">
		<table border="2" class="table table-dark">
			<thead>
				<tr>
					<td>N°</td>
					<td>Nombre</td>
					<td>Apellido</td>
					<td>Telefono</td>
					<td>Direccion</td>
					<td>Operacion</td>
				</tr>
			</thead>
			<tbody>
				<?php $n=1; foreach ($encargado as $e) {?>
					<tr>
						<td><?php echo $n; ?></td>
						<td><?php echo $e->nombre; ?></td>
						<td><?php echo $e->apellido; ?></td>
						<td><?php echo $e->telefono; ?></td>
						<td><?php echo $e->direccion; ?></td>
						<td><a href="{{Route('eliminar','$e->id_encargado')}}" class="btn btn-danger">Eliminar</a></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="">
        	<div class="row">
        		<div class="col-md-6">
        			<label for="">Ingrese nombre</label>
        			<div class="col-md-6">
        				<input type="text" name="nombre" id="nombre" class="form-control">
        			</div>
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
	</div>
	
