<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encargado extends Model
{
    public $timestamps = false;
    protected $table = 'encargado';
    protected $fillable =['nombre','apellido','telefono','direccion'];
}
