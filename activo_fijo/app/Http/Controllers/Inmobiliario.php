<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Encargado;
use App\Usuario;
use Illuminate\Support\Facades\DB;

class Inmobiliario extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $v=Encargado::all();
        echo view('template/header');
         echo view('template/navbar');
        return view('encargadoV',['encargado'=>$v]);
        echo view('template/footer');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data=request()->all();
        $enc= new Encargado;

        $enc->nombre=$data['nombre'];
        $enc->apellido=$data['apellido'];
        $enc->name=$data['telefono'];
        $enc->name=$data['direccion'];
        
        $enc->save();

        return redirect()->Route('inicio');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $v= $id->find($id);
        echo view('encargadoV',['encargados ' => $v]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /*$v = Encargado::find($id);
        $v->nombre = $request->get('nombre');
        $v->apellido = $request->('apellido');
        $v->telefono = $request->('telefono');
        $v->direccion = $request->('direccion');

        $v->save();

        return redirect()->Route('encargado')->with('success','Actualizado correctamente');*/
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('encargado')->where('id_encargado','=',$id)->delete();
        return redirect()->route('encargado')->with('success','Eliminado');
    }
}
